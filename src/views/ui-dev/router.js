import layer from './layer/index'

const data = [
  {
    path: '/layer',
    component: layer
  }
];
data.forEach(function (item) {
  item.path = '/ui-dev' + item.path;
});

export default data;

import kDate from './kDate'

const data = [
  {
    path: '/kDate',
    component: kDate
  }
];
data.forEach(function (item) {
  item.path = '/kx-js' + item.path;
});

export default data;

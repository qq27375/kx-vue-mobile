import Vue from 'vue'
import Router from 'vue-router'
import index from '@/views/index'
import kxjsRouter from '@/views/kx-js/router'
import devRouter from '@/views/ui-dev/router'

Vue.use(Router);

export default new Router({
  routes: [

    {
      path: '/',
      name: 'index',
      component: index
    },
    ...kxjsRouter,
    ...devRouter
  ]
})

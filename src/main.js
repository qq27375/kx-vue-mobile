import Vue from 'vue'
import App from './App'
import router from './router'
import {init} from 'kx-js'

Vue.config.productionTip = false;
init({});

Vue.prototype.$setTitle = function (title) {
  document.title = title;
};

new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
});
